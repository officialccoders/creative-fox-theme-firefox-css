# Creative Fox Theme - Firefox CSS
<li>(EN) A theme for Opera GX and Creative Fox Lovers.</li></ul>

# How to install?

<ol><b>(EN) Steps to configure the theme:</b>
   <li>Download <a href= "https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search">Tree Style Tabs</a></li>
   <li>Download <a href= "https://addons.mozilla.org/en-US/firefox/addon/darkreader/">Dark Reader</a></li>
   <li>Download the theme with the big button: <code>Code</code> >> <code>Download.zip</code></li> 
   <li>Copy the chrome folder and the user.js file into your Firefox profile directory. </li>
   <li>Restart the browser. </li></ol>
   <ul>

# Optional Themes
<ul><li>Main Theme: <a href= "https://addons.mozilla.org/en-US/firefox/addon/matte-black-theme/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search"> Matte Black (White)</a></li>

<ul><li>Other Main Themes: <a href= "https://addons.mozilla.org/es/firefox/addon/beautiful-opera-gx-fucsia/">🔴Fuchsia</a> <a href= "https://addons.mozilla.org/es/firefox/addon/beautiful-opera-gx-blue/">🔵Blue</a> <a href= "https://addons.mozilla.org/es/firefox/addon/beautiful-gx-green/" >🟢Green</a> <a href= "https://addons.mozilla.org/es/firefox/addon/beautiful-red-blur/">🌈Custom</a> </li>

<li>Other Themes: <a href= "https://addons.mozilla.org/es/firefox/addon/opera-gx-witchcraft-purple/">💜Purple</a> <a href= "https://addons.mozilla.org/es/firefox/addon/opera-gx-electric-aquamarine/">💚Aquamarine</a> <a href= "https://addons.mozilla.org/es/firefox/addon/opera-gx-ember-orange/">🦧Orange</a> <a href= "https://addons.mozilla.org/es/firefox/addon/opera-gx-frozen-cyan/">💠Cyan</a> <a href= "https://addons.mozilla.org/es/firefox/addon/opera-gx-level-up-green/">🍏GreenLight</a> <a href= "https://addons.mozilla.org/es/firefox/addon/opera-gx-stamina-yellow/">💛Yellow</a> <a href= "https://addons.mozilla.org/es/firefox/addon/opera-gx-wizard-grey/"> 🗻Gray</a></li>
   <li><a href="https://addons.mozilla.org/es/firefox/addon/opera-light/">White Theme ⚪</a></li></ul>
